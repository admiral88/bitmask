﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BitMask
{
    [Flags]
    public enum Roles
    {
        None = 0x0,
        Guest = 0x1,
        User = 0x2,
        Moderator = 0x4,
        Manager = 0x8,
        Admin = 0x10
    }

}
