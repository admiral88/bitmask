﻿using System;

namespace BitMask
{
    public class TestService
    {
        public void Start()
        {
            var user = new User { Name = "Tom", Role = Roles.None };

            user.Role = SetRole(user.Role, Roles.User);
            user.Role = SetRole(user.Role, Roles.Moderator);
            user.Role = SetRole(user.Role, Roles.Admin);
            AllowRoleUserModerator(user);
            user.Role = UnSetRole(user.Role, Roles.Admin);
            AllowRoleUserModerator(user);
            user.Role = UnSetRole(user.Role, Roles.User);
            AllowRoleUserModerator(user);

            Console.ReadLine();
        }



        private void AllowRoleUserModerator(User user)
        {
            var allowRoles = Roles.User | Roles.Moderator;
            if((user.Role & allowRoles) == allowRoles)
            {
                Console.WriteLine("AllowRoleUserModerator executed");
                return;
            }
            Console.WriteLine($"AllowRoleUserModerator - {user.Name} does not have permitions");
        }




        private bool IsRole(Roles userRoles, Roles flag)
        {
            var userRolesValue = (int)userRoles;
            var flagValue = (int)flag;
            return (userRolesValue & flagValue) != 0;
        }

        private Roles SetRole(Roles userRoles, Roles flag)
        {
            var newUserRoles = userRoles | flag;
            return newUserRoles;
        }

        private Roles UnSetRole(Roles userRoles, Roles flag)
        {
            var newUserRoles = userRoles & (~flag);
            return newUserRoles;
        }

    }
}
